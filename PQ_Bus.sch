EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "PQ9ISH COMMS V/U"
Date "2019-11-26"
Rev "0.9.5"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lsf-kicad:PQ9-Connector PQ1
U 1 1 5A2897BB
P 9150 5100
AR Path="/5A2897BB" Ref="PQ1"  Part="1" 
AR Path="/5A289551/5A2897BB" Ref="PQ1"  Part="1" 
F 0 "PQ1" H 9150 5600 50  0000 C CNN
F 1 "PQ9-Connector" V 9450 5100 50  0000 C CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x09_P2.00mm_Vertical" H 9150 5100 50  0001 C CNN
F 3 "" H 9150 5100 50  0001 C CNN
F 4 "SQT-109-03-LS" H 9150 5100 50  0001 C CNN "Part Number"
	1    9150 5100
	1    0    0    -1  
$EndComp
Text HLabel 8700 4700 0    60   BiDi ~ 0
1-Wire|Opt1
$Comp
L power:GND #PWR038
U 1 1 5A289804
P 9250 5800
F 0 "#PWR038" H 9250 5550 50  0001 C CNN
F 1 "GND" H 9250 5650 50  0000 C CNN
F 2 "" H 9250 5800 50  0001 C CNN
F 3 "" H 9250 5800 50  0001 C CNN
	1    9250 5800
	1    0    0    -1  
$EndComp
Text HLabel 4700 2500 0    60   Input ~ 0
CAN1TX
Text HLabel 4700 2600 0    60   Output ~ 0
CAN1RX
$Comp
L power:GND #PWR039
U 1 1 5A7B673E
P 6550 3100
F 0 "#PWR039" H 6550 2850 50  0001 C CNN
F 1 "GND" H 6550 2950 50  0000 C CNN
F 2 "" H 6550 3100 50  0001 C CNN
F 3 "" H 6550 3100 50  0001 C CNN
	1    6550 3100
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4148W D1
U 1 1 5A7BB63E
P 6300 4000
F 0 "D1" H 6300 4100 50  0000 C CNN
F 1 "1N4148W" H 6300 3900 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6300 4000 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" H 6300 4000 50  0001 C CNN
F 4 "1N4148W-E3-08" H 0   0   50  0001 C CNN "Part Number"
F 5 "SingleCAN" H 6300 4000 50  0001 C CNN "Variant"
	1    6300 4000
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP4
U 1 1 5A7BB9DD
P 5550 3650
F 0 "JP4" V 5550 3718 50  0000 L CNN
F 1 "SolderJumper_2_Open" V 5595 3718 50  0001 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" V 5480 3650 50  0001 C CNN
F 3 "~" H 5550 3650 50  0001 C CNN
	1    5550 3650
	0    1    1    0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP3
U 1 1 5A7BBA5A
P 5400 3650
F 0 "JP3" V 5400 3450 50  0000 L CNN
F 1 "SolderJumper_2_Open" V 5445 3718 50  0001 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" V 5330 3650 50  0001 C CNN
F 3 "~" H 5400 3650 50  0001 C CNN
	1    5400 3650
	0    1    1    0   
$EndComp
Text HLabel 4700 2800 0    60   Input ~ 0
CanEn
$Comp
L Device:R R10
U 1 1 5A7BE651
P 7100 4100
F 0 "R10" V 7180 4100 50  0000 C CNN
F 1 "3.3k" V 7100 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7030 4100 50  0001 C CNN
F 3 "" H 7100 4100 50  0001 C CNN
F 4 "MCT06030C3301FP500" H 0   0   50  0001 C CNN "Part Number"
F 5 "SingleCAN" V 7100 4100 50  0001 C CNN "Variant"
	1    7100 4100
	1    0    0    -1  
$EndComp
Text Notes 6650 3400 0    60   ~ 0
Driverless CAN
Wire Wire Line
	7050 2600 7800 2600
Wire Wire Line
	7800 2600 7800 3900
Wire Wire Line
	7050 2800 7500 2800
Wire Wire Line
	7500 2800 7500 3900
Connection ~ 7500 4900
Wire Wire Line
	4700 2500 5550 2500
Wire Wire Line
	4700 2600 5400 2600
Wire Wire Line
	6550 1550 6550 1650
Wire Wire Line
	6450 4000 6800 4000
Wire Wire Line
	6800 4000 6800 4150
Wire Wire Line
	6150 4000 5550 4000
Connection ~ 5550 2500
Wire Wire Line
	5400 4150 6800 4150
Connection ~ 6800 4150
Connection ~ 5400 2600
Wire Wire Line
	5400 2600 5400 3500
Wire Wire Line
	5400 3800 5400 4150
Wire Wire Line
	5550 4000 5550 3800
Wire Wire Line
	5550 3500 5550 2500
Wire Wire Line
	7100 4250 7100 4900
Connection ~ 7100 4900
Wire Wire Line
	7100 3800 7100 3950
Wire Notes Line
	5200 3400 7300 3400
Wire Notes Line
	7300 4350 5200 4350
Wire Notes Line
	5200 4350 5200 3400
Wire Notes Line
	7300 3400 7300 4350
Wire Wire Line
	6800 4900 7100 4900
$Comp
L power:GND #PWR044
U 1 1 5A84FC99
P 9050 5800
F 0 "#PWR044" H 9050 5550 50  0001 C CNN
F 1 "GND" H 9050 5650 50  0000 C CNN
F 2 "" H 9050 5800 50  0001 C CNN
F 3 "" H 9050 5800 50  0001 C CNN
	1    9050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4900 8750 4900
Wire Wire Line
	5550 2500 6050 2500
Wire Wire Line
	6800 4150 6800 4900
Wire Wire Line
	5400 2600 6050 2600
Wire Wire Line
	7100 4900 7500 4900
Wire Wire Line
	8700 4700 8750 4700
NoConn ~ 8750 5200
NoConn ~ 8750 5100
$Comp
L power:VCCQ #PWR046
U 1 1 5A876E7F
P 6550 1550
F 0 "#PWR046" H 6550 1400 50  0001 C CNN
F 1 "VCCQ" H 6550 1700 50  0000 C CNN
F 2 "" H 6550 1550 50  0001 C CNN
F 3 "" H 6550 1550 50  0001 C CNN
	1    6550 1550
	1    0    0    -1  
$EndComp
$Comp
L power:VCCQ #PWR047
U 1 1 5A876E9A
P 7100 3800
F 0 "#PWR047" H 7100 3650 50  0001 C CNN
F 1 "VCCQ" H 7100 3950 50  0000 C CNN
F 2 "" H 7100 3800 50  0001 C CNN
F 3 "" H 7100 3800 50  0001 C CNN
	1    7100 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4800 8750 4800
Text Label 8050 4900 0    50   ~ 0
D+
$Comp
L Device:R R9
U 1 1 5A96C4BC
P 7650 3900
F 0 "R9" V 7550 3900 50  0000 C CNN
F 1 "120" V 7650 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7580 3900 50  0001 C CNN
F 3 "~" H 7650 3900 50  0001 C CNN
F 4 "CRCW0603120RFKEA" H 0   0   50  0001 C CNN "Part Number"
F 5 "Place only on terminal devices" V 7650 3900 50  0001 C CNN "Comment"
F 6 "Full CAN" V 7650 3900 50  0001 C CNN "Variant"
	1    7650 3900
	0    1    1    0   
$EndComp
Connection ~ 7800 3900
Connection ~ 7500 3900
Wire Wire Line
	7500 3900 7500 4900
Wire Wire Line
	6850 5350 6650 5350
Wire Wire Line
	9050 5700 9050 5800
Wire Wire Line
	9250 5800 9250 5700
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 5CE7D6D1
P 7000 5350
F 0 "JP1" H 7000 5555 50  0000 C CNN
F 1 "v in selector" H 7000 5464 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 7000 5350 50  0001 C CNN
F 3 "~" H 7000 5350 50  0001 C CNN
	1    7000 5350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5CE80A63
P 7900 5300
F 0 "#PWR0110" H 7900 5150 50  0001 C CNN
F 1 "+5V" V 7915 5428 50  0000 L CNN
F 2 "" H 7900 5300 50  0001 C CNN
F 3 "" H 7900 5300 50  0001 C CNN
	1    7900 5300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 5350 7500 5000
Text Label 8050 5000 0    50   ~ 0
V1_3.3
$Comp
L Interface_CAN_LIN:TCAN334 U6
U 1 1 5CE72771
P 6550 2700
F 0 "U6" H 6200 3250 50  0000 C CNN
F 1 "TCAN334" H 6300 3150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-8" H 6550 2200 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tcan337.pdf" H 6550 2700 50  0001 C CNN
F 4 "TCAN334GDCNT" H 6550 2700 50  0001 C CNN "Part Number"
F 5 "Full CAN" H 6550 2700 50  0001 C CNN "Variant"
	1    6550 2700
	1    0    0    -1  
$EndComp
Text HLabel 4700 2900 0    60   Input ~ 0
CAN_STB
Wire Wire Line
	4700 2900 6050 2900
Wire Wire Line
	4700 2800 5100 2800
$Comp
L Device:C C39
U 1 1 5CE926AF
P 6850 1800
F 0 "C39" H 6965 1846 50  0000 L CNN
F 1 "100nF" H 6965 1755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6888 1650 50  0001 C CNN
F 3 "~" H 6850 1800 50  0001 C CNN
F 4 "C0603C104K9RACTU" H 6850 1800 50  0001 C CNN "Part Number"
F 5 "Full CAN" H 6850 1800 50  0001 C CNN "Variant"
	1    6850 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5CE92D75
P 6850 2050
F 0 "#PWR0113" H 6850 1800 50  0001 C CNN
F 1 "GND" H 6855 1877 50  0000 C CNN
F 2 "" H 6850 2050 50  0001 C CNN
F 3 "" H 6850 2050 50  0001 C CNN
	1    6850 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 2050 6850 1950
Wire Wire Line
	6850 1650 6550 1650
Connection ~ 6550 1650
Wire Wire Line
	6550 1650 6550 2300
Wire Wire Line
	7500 5000 8750 5000
Text Notes 5050 5850 0    50   ~ 0
JP1\nOpen: For internal 3.3V regulator\nClosed: For external 3.3V (V1). LM1117 should not be populated.
Wire Notes Line
	7600 5100 7600 5900
Wire Notes Line
	7600 5900 5000 5900
Wire Notes Line
	5000 5900 5000 5100
Wire Notes Line
	5000 5100 7600 5100
Text Notes 7900 3900 0    50   ~ 0
Termination resistance.\nOnly on outmost bus subsystems
$Comp
L power:+3.3V #PWR0102
U 1 1 5D8814E5
P 6650 5350
F 0 "#PWR0102" H 6650 5200 50  0001 C CNN
F 1 "+3.3V" V 6665 5478 50  0000 L CNN
F 2 "" H 6650 5350 50  0001 C CNN
F 3 "" H 6650 5350 50  0001 C CNN
	1    6650 5350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 5D864F70
P 5100 1950
F 0 "R8" H 5170 1996 50  0000 L CNN
F 1 "10k" H 5170 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 1950 50  0001 C CNN
F 3 "~" H 5100 1950 50  0001 C CNN
F 4 "RE0603BRE0710KL" H 5100 1950 50  0001 C CNN "Part Number"
F 5 "Full CAN" H 5100 1950 50  0001 C CNN "Variant"
	1    5100 1950
	1    0    0    -1  
$EndComp
$Comp
L power:VCCQ #PWR0121
U 1 1 5D865616
P 5100 1550
F 0 "#PWR0121" H 5100 1400 50  0001 C CNN
F 1 "VCCQ" H 5100 1700 50  0000 C CNN
F 2 "" H 5100 1550 50  0001 C CNN
F 3 "" H 5100 1550 50  0001 C CNN
	1    5100 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2800 5100 2100
Wire Wire Line
	5100 1800 5100 1550
Connection ~ 5100 2800
Wire Wire Line
	5100 2800 6050 2800
Wire Wire Line
	7900 5300 8750 5300
Wire Wire Line
	7150 5350 7500 5350
Wire Wire Line
	7800 4550 7800 4800
Wire Wire Line
	7800 3900 7800 4550
Connection ~ 7800 4550
Wire Wire Line
	7900 4550 7800 4550
Text HLabel 8350 4550 2    50   BiDi ~ 0
D-|Opt2
Wire Wire Line
	8350 4550 8200 4550
$Comp
L Device:R R15
U 1 1 5DD7F3A9
P 8050 4550
F 0 "R15" V 7843 4550 50  0000 C CNN
F 1 "0" V 7934 4550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7980 4550 50  0001 C CNN
F 3 "~" H 8050 4550 50  0001 C CNN
	1    8050 4550
	0    1    1    0   
$EndComp
$EndSCHEMATC
